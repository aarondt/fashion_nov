
from keras import backend
import numpy as np

import io
from PIL import Image
import cv2
import keras
# from keras import backend as K
# from keras.models import Sequential
# from keras.layers import Activation
# # from keras.layers.core import Dense, Flatten
# from keras.optimizers import Adam, RMSprop
# from keras.metrics import categorical_crossentropy
# from keras.preprocessing.image import ImageDataGenerator, img_to_array
# from keras.layers.normalization import BatchNormalization
# from keras.layers.convolutional import *
# from keras.models import load_model
#
# from keras.preprocessing.image import ImageDataGenerator
# from keras.models import Sequential
# from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
# from keras import applications
# from keras.models import Model

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

from flask import Flask
from flask_bcrypt import Bcrypt

from flask_login import LoginManager
from keras.models import load_model



### Database Stuff ----------
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

config = tf.ConfigProto(
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
    # device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
set_session(session)



app = Flask(__name__)

app.config['SECRET_KEY'] = 'afcf4b7838c84b603585898bc8c01805' #  Secretkey mit secrets selbst generiert.
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db' # location der SQLite Datei mit relativem Path angegeben

###SQLALCHEMY
db = SQLAlchemy(app) #create database instance - structure is represented as classes called models
# each class is its own table in the database, lets start with user
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'


print("* Loading models...")
#def get_model():
global model
global arm_model
global muster_model
global maincat_model
# GLOBAL DEFINIEREN!!
model = load_model("./models/final_model.h5")
arm_model = None#load_model("./models/arm_model.h5")
muster_model = None#load_model("./models/mobileNet_32Dense_muster.h5")
maincat_model = load_model("./models/Hauptklassen.h5")

#get_model()
print(" * * * * * COMPLETE: models loaded!")

from predict_app import routes
from predict_app import helper_functions
