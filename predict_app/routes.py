

from flask import url_for, render_template, flash, redirect
from flask import request
from flask import jsonify
from predict_app import app, db, bcrypt
from datetime import datetime
import io
from PIL import Image
import cv2
import numpy as np

#Import from forms.py
from predict_app.forms import RegistrationForm, LoginForm, FileUploadForm, UploadForm, TaxonomieForm
import predict_app.farberkennung as farberkennung
#import helperfunctions
from predict_app.helper_functions import preprocess_image, decode_base64##,get_model
import base64
from predict_app.models import User, Upload, Item_Details, OriginalTaxonomie, Predictions
from flask_login import login_user, current_user, logout_user, login_required
import os
#import secrets
from os import urandom
from PIL import Image
from predict_app import model, arm_model, muster_model, maincat_model
import math
import flask
from keras.preprocessing.image import ImageDataGenerator


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        os.mkdir(os.path.join(app.root_path, 'static/image_uploads', str(user.id)+"_files"))
        flash('Account für {} wurde erfolgreich angelegt!'.format(user.username), 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)



@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))

# Berechne die Size eines Files
def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

@app.route("/account")
@login_required
def account():
	form = RegistrationForm()
	#user_memory_usage = sum(os.path.getsize(f) for f in os.listdir(os.path.join(app.root_path, 'static/image_uploads', str(current_user.id)+"_files")) if os.path.isfile(f))
	user_memory_usage = convert_size(get_size(os.path.join(app.root_path, 'static/image_uploads', str(current_user.id)+"_files")))
	anzahl_files = len(os.listdir(os.path.join(app.root_path, 'static/image_uploads', str(current_user.id)+"_files")))
	print('**** memory used: ', user_memory_usage)
	form.email.data =  current_user.email
	form.username.data = current_user.username
	return render_template('account.html', title='Account', form=form, disk_usage=user_memory_usage, file_count=anzahl_files)

### Bilder hochladen und speichern
@app.route('/upload', methods=['GET', 'POST'])
def upload():
    form = UploadForm()
    if form.validate_on_submit() and 'photo' in request.files:
        for f in request.files.getlist('photo'):
            random_hex = urandom(8).hex()#secrets.token_hex(8)
            _, f_ext = os.path.splitext(f.filename)
            picture_fn = random_hex + f_ext

            #filename = secure_filename(f.filename)

            f.save(os.path.join(app.root_path, 'static/image_uploads', str(current_user.id)+"_files", picture_fn))
            #f.save(os.path.join(app.root_path, 'static/image_uploads', picture_fn))
            upload = Upload(filename=picture_fn,date_posted=datetime.utcnow(), user_id=current_user.id)
            db.session.add(upload)
        db.session.commit()
        return 'Upload completed.'
    return render_template('upload.html', form=form)




#def save_picture(form_picture):
#
#
#    random_hex = urandom(8).hex()#secrets.token_hex(8)
#    _, f_ext = os.path.splitext(form_picture.filename)
#    picture_fn = random_hex + f_ext
#    picture_path = os.path.join(app.root_path, 'static/image_uploads', picture_fn)
#    print("saving image under: ", picture_path)
#    output_size = (125, 125)
#    i = Image.open(form_picture)
#    i.thumbnail(output_size)
#    i.save(picture_path)
#
#    return picture_fn
#
#
#@app.route("/upload",methods=['GET', 'POST'])
#def upload():
#	form = FileUploadForm()
#	if form.validate_on_submit():
#		if form.picture.data:
#			for f in form.picture.data:
#				picture_file = save_picture(f)
#            #current_user.image_file = picture_file
#        #db.session.commit()
#		flash('Bild wurde hochgeladen!', 'success')
#	return render_template('upload.html', title='upload', form=form)

@app.route('/database')
@login_required
def database():
	user_files = current_user.uploads
	user_items = current_user.items#Item_Details.query.all()
	original_items = OriginalTaxonomie.query.all()
	items = zip(user_items,original_items)

	return render_template('database.html', title='Database', data=user_files, item_database =items)

@app.route('/items/<int:item_id>', methods=['GET','POST'])
@login_required
def items(item_id):
	item = Item_Details.query.get_or_404(item_id)
	form = TaxonomieForm()

	if form.validate_on_submit():
		item.item_name = form.title.data
		item.item_category = form.category.data
		db.session.commit()
		flash('Taxonomie wurde erfolgreich bearbeitet!', 'success')
		return redirect(url_for('items', item_id=item.id))
	elif request.method == "GET":
		form.title.data = item.item_name
		form.category.data = item.item_category
	return render_template('item.html', title=item.item_name, item=item, form=form)


@app.route('/')
@login_required
def home():
	# __file__ refers to the file settings.py

	#print("*** files:",os.listdir(app.instance_path))
	## display image funktionen -> stackoverflow
	flask.session['count'] = 0
	_files = [i.filename for i in current_user.uploads]
	return render_template('dashboard.html', title='Dashboard', photo = _files[0])

# photo route

#  Predictions aus Datenbank auslesen
@app.route('/get_photo', methods=['GET'])
def get_photo():
    _direction = flask.request.args.get('direction')
    flask.session['count'] = flask.session['count'] + (1 if _direction == 'f' else - 1)
    _files = [i.filename for i in current_user.uploads]
    current_filename_to_display =  '1_files/'+str(_files[flask.session['count']])
    print(current_filename_to_display)
    predictions = Predictions.query.filter_by(filename=current_filename_to_display).first()
    #print(" * * * * *  matching prediction: ", str(matching_prediction))
    response = {
        'prediction': {
                '34-hosen':predictions.drei_viertel_hosen,
                 'Abendkleider':predictions.Abendkleider,
                 'Blusenkleider':predictions.Blusenkleider,
                 'Brautkleider':predictions.Brautkleider,
                 'Bundfaltenhosen':predictions.Bundfaltenhosen,
                 'Business-Hosen':predictions.Business_Hosen,
                 'Chinos':predictions.Chinos,
                 'Cocktailkleider':predictions.Cocktailkleider,
                 'Cordhosen':predictions.Cordhosen,
                 'Culottes':predictions.Culottes,
                 'Etuikleider':predictions.Etuikleider,
                 'Haremshosen':predictions.Haremshosen,
                 'Jeanskleider':predictions.Jeanskleider,
                 'Jerseykleider':predictions.Jerseykleider,
                 'Lederhosen':predictions.Lederhosen,
                 'Leggings':predictions.Leggings,
                 'Marlenehosen':predictions.Marlenehosen,
                 'Parka':predictions.Parka,
                 'Shorts':predictions.Shorts,
                 'Sommerkleider':predictions.Sommerkleider,
                 'Spitzenkleider':predictions.Spitzenkleider,
                 'Steppjacken':predictions.Steppjacken,
                 'Stoffhosen':predictions.Stoffhosen,
                 'Strickkleider':predictions.Strickkleider,
                 'Sweatpants':predictions.Sweatpants,
                 'blousons':predictions.blousons,
                 'bomberjacken':predictions.bomberjacken,
                 'cargojacken':predictions.cargojacken,
                 'jeansjacken':predictions.jeansjacken,
                 'lederjacken':predictions.lederjacken,
                 'outdoorjacken':predictions.outdoorjacken,
                 'regenjacken':predictions.regenjacken,
            },

                 #### Hauptkategorien
            'main_cat': {
                 'jacken'	:	predictions.jacken	,
                 'kleider'	:	predictions.kleider	,
                 'jeans'	:	predictions.jeans	,
                 'strick'	:	predictions.strick	,
                 'maentel'	:	predictions.maentel	,
                 'blusen_und_tuniken'	:	predictions.blusen_und_tuniken	,
                 'hosen'	:	predictions.hosen	,
                 'shirts'	:	predictions.shirts	,
                 'sweat'	:	predictions.sweat	,
                 'pullover'	:	predictions.pullover	,
                 'waesche'	:	predictions.waesche	,
                 'roecke'	:	predictions.roecke	,
                 'blazer'	:	predictions.blazer	,
                 'bademode'	:	predictions.bademode	,
                 'jumpsuits_und_overalls'	:	predictions.jumpsuits_und_overalls	,
                 'tops'	:	predictions.tops	,
                 'westen'	:	predictions.westen	,
                 'shorts_hose'	:	predictions.shorts_hose	,
                 'ponchos_und_kimonos'	:	predictions.ponchos_und_kimonos
                 }
                }

    print(response)

    return flask.jsonify({'photo':_files[flask.session['count']], 'forward':str(flask.session['count']+1 < len(_files)), 'back':str(bool(flask.session['count'])), 'predictions': response})

def return_classes(prediction,erkannte_farbe,rbg_farbwert,arm_prediction,muster_prediction,maincat_prediction):
		response = {
			# inside response we have another dict holding all the predictions
			'prediction': {
					'34-hosen': prediction[0][0],
					 'Abendkleider':  prediction[0][1],
					 'Blusenkleider':  prediction[0][2],
					 'Brautkleider':  prediction[0][3],
					 'Bundfaltenhosen': prediction[0][4],
					 'Business-Hosen':  prediction[0][5],
					 'Chinos':  prediction[0][6],
					 'Cocktailkleider':  prediction[0][7],
					 'Cordhosen':  prediction[0][8],
					 'Culottes':  prediction[0][9],
					 'Etuikleider':  prediction[0][10],
					 'Haremshosen':  prediction[0][11],
					 'Jeanskleider':  prediction[0][12],
					 'Jerseykleider':  prediction[0][13],
					 'Lederhosen':  prediction[0][14],
					 'Leggings':  prediction[0][15],
					 'Marlenehosen':  prediction[0][16],
					 'Parka':  prediction[0][17],
					 'Shorts':  prediction[0][18],
					 'Sommerkleider':  prediction[0][19],
					 'Spitzenkleider':  prediction[0][20],
					 'Steppjacken':  prediction[0][21],
					 'Stoffhosen':  prediction[0][22],
					 'Strickkleider':  prediction[0][23],
					 'Sweatpants':  prediction[0][24],
					 'blousons':  prediction[0][25],
					 'bomberjacken':  prediction[0][26],
					 'cargojacken':  prediction[0][27],
					 'jeansjacken':  prediction[0][28],
					 'lederjacken':  prediction[0][29],
					 'outdoorjacken':  prediction[0][30],
					 'regenjacken':  prediction[0][31]
					 },
			'farbe':{'Top1': erkannte_farbe[0],
						'Top2': erkannte_farbe[1],
						'Top3': erkannte_farbe[2]
					},
			'rgb_farbwerte': {'Rgb-werte': rbg_farbwert},

			'arm_length': {

					'Aermellos': arm_prediction[0][0],
					 'Dreiviertelarm':  arm_prediction[0][1],
					 'Halbarm':  arm_prediction[0][2],
					 'Langarm':  arm_prediction[0][3],
					 'Viertelarm': arm_prediction[0][4]
			},

			'muster': {

						'Animalprint': muster_prediction[0][0],
						'Camouflage': muster_prediction[0][1],
						'Ethno_Muster': muster_prediction[0][2],
						'Farbverlauf': muster_prediction[0][3],
						'Gebluemt_floral': muster_prediction[0][4],
						'Gepunktet': muster_prediction[0][5],
						'Gestreift': muster_prediction[0][6],
						'Kariert': muster_prediction[0][7],
						'Logoprint': muster_prediction[0][8],
						'Melange': muster_prediction[0][9],
						'Motivprint':muster_prediction[0][10],
						'Mottoprint':muster_prediction[0][11],
						'Norweger_Muster':muster_prediction[0][12],
						'Paisley_Muster':muster_prediction[0][13],
						'Two_Tone':muster_prediction[0][14],
						'Unifarben':muster_prediction[0][15]

			},

			'main_cat': {
				'jacken': maincat_prediction[0][0],
				 'kleider': maincat_prediction[0][1],
				 'jeans': maincat_prediction[0][2],
				 'strick': maincat_prediction[0][3],
				 'maentel': maincat_prediction[0][4],
				 'blusen-und-tuniken': maincat_prediction[0][5],
				 'hosen': maincat_prediction[0][6],
				 'shirts': maincat_prediction[0][7],
				 'sweat': maincat_prediction[0][8],
				 'pullover': maincat_prediction[0][9],
				 'waesche': maincat_prediction[0][10],
				 'roecke': maincat_prediction[0][11],
				 'blazer': maincat_prediction[0][12],
				 'bademode': maincat_prediction[0][13],
				 'jumpsuits-und-overalls': maincat_prediction[0][14],
				 'tops': maincat_prediction[0][15],
				 'westen': maincat_prediction[0][16],
				 'shorts': maincat_prediction[0][17],
				 'ponchos-und-kimonos': maincat_prediction[0][18]



			}
		}
		return response


@app.route('/dashboard-batch', methods=["GET"]) #Send image data to end point for prediction
def predict_batch():
	print("* called predict")
	datagen = ImageDataGenerator(
	rescale = 1./255
	)
	APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
	APP_STATIC = os.path.join(APP_ROOT, '/static/image_uploads/1_files')
	print("+ + + + static:",APP_STATIC)
	print("+ + + + static:",APP_ROOT+ APP_STATIC)
	predict_gen= datagen.flow_from_directory(
		'/Users/aarontal/Desktop/prototype/predict_app/static/image_uploads/',
		target_size = (224,224),
		batch_size = 1,
		shuffle=False

	)

	#batch_prediction = model.predict_generator(predict_gen, steps=len(current_user.uploads),verbose=1)
	prediction = model.predict_generator(predict_gen, steps=len(current_user.uploads),verbose=1)# model is global and already inizialized at the top of this program
	#arm_prediction = arm_model.predict_generator(predict_gen, steps=len(current_user.uploads),verbose=1) # armModell
	#muster_prediction = muster_model.predict_generator(predict_gen, steps=len(current_user.uploads),verbose=1) # Modell
	maincat_prediction = maincat_model.predict_generator(predict_gen, steps=len(current_user.uploads),verbose=1) # Modell


    # Speichere Predictions
	for i in range(0,len(prediction)):
		predictions_entry = Predictions(filename=predict_gen.filenames[i], drei_viertel_hosen= float(prediction[i][0]),
							 Abendkleider=  float(prediction[i][1]),
							 Blusenkleider=  float(prediction[i][2]),
							 Brautkleider=  float(prediction[i][3]),
							 Bundfaltenhosen= float(prediction[i][4]),
							 Business_Hosen=  float(prediction[i][5]),
							 Chinos=  float(prediction[i][6]),
							 Cocktailkleider=  float(prediction[i][7]),
							 Cordhosen=  float(prediction[i][8]),
							 Culottes=  float(prediction[i][9]),
							 Etuikleider=  float(prediction[i][10]),
							 Haremshosen=  float(prediction[i][11]),
							 Jeanskleider=  float(prediction[i][12]),
							 Jerseykleider=  float(prediction[i][13]),
							 Lederhosen=  float(prediction[i][14]),
							 Leggings=  float(prediction[i][15]),
							 Marlenehosen=  float(prediction[i][16]),
							 Parka=  float(prediction[i][17]),
							 Shorts=  float(prediction[i][18]),
							 Sommerkleider=  float(prediction[i][19]),
							 Spitzenkleider=  float(prediction[i][20]),
							 Steppjacken=  float(prediction[i][21]),
							 Stoffhosen=  float(prediction[i][22]),
							 Strickkleider=  float(prediction[i][23]),
							 Sweatpants=  float(prediction[i][24]),
							 blousons=  float(prediction[i][25]),
							 bomberjacken=  float(prediction[i][26]),
							 cargojacken=  float(prediction[i][27]),
							 jeansjacken=  float(prediction[i][28]),
							 lederjacken=  float(prediction[i][29]),
							 outdoorjacken=  float(prediction[i][30]),
							 regenjacken=  float(prediction[i][31]),

                            #Hauptkategorie
                             jacken=float( maincat_prediction[i][0]),
                             kleider=float( maincat_prediction[i][1]),
                             jeans=float( maincat_prediction[i][2]),
                             strick=float( maincat_prediction[i][3]),
                             maentel=float( maincat_prediction[i][4]),
                             blusen_und_tuniken=float( maincat_prediction[i][5]),
                             hosen=float( maincat_prediction[i][6]),
                             shirts=float( maincat_prediction[i][7]),
                             sweat=float( maincat_prediction[i][8]),
                             pullover=float( maincat_prediction[i][9]),
                             waesche=float( maincat_prediction[i][10]),
                             roecke=float( maincat_prediction[i][11]),
                             blazer=float( maincat_prediction[i][12]),
                             bademode=float( maincat_prediction[i][13]),
                             jumpsuits_und_overalls=float( maincat_prediction[i][14]),
                             tops=float( maincat_prediction[i][15]),
                             westen=float( maincat_prediction[i][16]),
                             shorts_hose=float( maincat_prediction[i][17]),
                             ponchos_und_kimonos=float( maincat_prediction[i][18])
                             )
		db.session.add(predictions_entry)
		db.session.commit()
	#response = return_classes(prediction[0],erkannte_farbe[0],rbg_farbwert[0],arm_prediction[0],muster_prediction[0],maincat_prediction[0])
	#flask.session['prediction'] = prediction
	# print("*** PAST SESSION")
	# print("one pred:",prediction[0][0])
	# response = {}
	# response['prediction'] = {
	# 			'drei_viertel_hosen': float(prediction[0][0]),
	# 			 'Abendkleider':  float(prediction[0][1]),
	# 			 'Blusenkleider':  float(prediction[0][2]),
	# 			 'Brautkleider':  float(prediction[0][3]),
	# 			 'Bundfaltenhosen': float(prediction[0][4]),
	# 			 'Business-Hosen':  float(prediction[0][5]),
	# 			 'Chinos':  float(prediction[0][6]),
	# 			 'Cocktailkleider':  float(prediction[0][7]),
	# 			 'Cordhosen':  float(prediction[0][8]),
	# 			 'Culottes':  float(prediction[0][9]),
	# 			 'Etuikleider':  float(prediction[0][10]),
	# 			 'Haremshosen':  float(prediction[0][11]),
	# 			 'Jeanskleider':  float(prediction[0][12]),
	# 			 'Jerseykleider':  float(prediction[0][13]),
	# 			 'Lederhosen':  float(prediction[0][14]),
	# 			 'Leggings':  float(prediction[0][15]),
	# 			 'Marlenehosen':  float(prediction[0][16]),
	# 			 'Parka':  float(prediction[0][17]),
	# 			 'Shorts':  float(prediction[0][18]),
	# 			 'Sommerkleider':  float(prediction[0][19]),
	# 			 'Spitzenkleider':  float(prediction[0][20]),
	# 			 'Steppjacken':  float(prediction[0][21]),
	# 			 'Stoffhosen':  float(prediction[0][22]),
	# 			 'Strickkleider':  float(prediction[0][23]),
	# 			 'Sweatpants':  float(prediction[0][24]),
	# 			 'blousons':  float(prediction[0][25]),
	# 			 'bomberjacken':  float(prediction[0][26]),
	# 			 'cargojacken':  float(prediction[0][27]),
	# 			 'jeansjacken':  float(prediction[0][28]),
	# 			 'lederjacken':  float(prediction[0][29]),
	# 			 'outdoorjacken':  float(prediction[0][30]),
	# 			 'regenjacken':  float(prediction[0][31])
	# 			 }
	# print(response)
	#response = prediction[0]
	#return flask.jsonify({'photo':_files[flask.session['count']], 'forward':str(flask.session['count']+1 < len(_files)), 'back':str(bool(flask.session['count']))})

	return "Success"#jsonify(response)


@app.route('/dashboard', methods=["POST"]) #Send image data to end point for prediction
def predict():
	print("* called predict")
	message = request.get_json(force=True) #get data from POST
	encoded = message['image'] #access key 'image' -> image data is encoded in json
	encoded = encoded.replace("data:image/png;base64,","")
	decoded = base64.b64decode(encoded) # hence decode it!

	image = Image.open(io.BytesIO(decoded)) # here we create an Image-object and initialize is it our binarized image-data from before
	preprossed_image = preprocess_image(image, target_size=(224,224)) #now we can preprocess the image
	#preprossed_image_non_normed = preprocess_image(image, target_size=(224,224))
	prediction = model.predict(preprossed_image).tolist() # model is global and already inizialized at the top of this program
	arm_prediction = arm_model.predict(preprossed_image).tolist() # armModell
	muster_prediction = muster_model.predict(preprossed_image).tolist() # Modell
	maincat_prediction = maincat_model.predict(preprossed_image).tolist() # Modell
	## Convert PIL zu cv2 Format
	open_cv_image = np.array(image)
	erkannte_farbe, rbg_farbwert = farberkennung.run_farberkennung(open_cv_image)

	# .tolist() converts the array to a python list (required for the jsonify)
	# create a dict holding a prediction dict
	response = return_classes(prediction,erkannte_farbe,rbg_farbwert,arm_prediction,muster_prediction,maincat_prediction)

	return jsonify(response)


@app.route('/predictions')
@login_required
def predictions():
	all_predictions = Predictions.query.all()


	return render_template('predictions.html', title='Predictions', db_predictions =all_predictions)
