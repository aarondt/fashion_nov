import numpy as np
import base64
import io
from PIL import Image
import cv2
# import keras
# from keras import backend as K
# from keras.models import Sequential
# from keras.layers import Activation
# from keras.layers.core import Dense, Flatten
# from keras.optimizers import Adam, RMSprop
# from keras.metrics import categorical_crossentropy
# from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array
# from keras.layers.normalization import BatchNormalization
# from keras.layers.convolutional import *

#
# from keras.preprocessing.image import ImageDataGenerator
# from keras.models import Sequential
# from keras.layers import Dropout, Flatten, Dense
# from keras import applications

# 
# from keras.layers import Dense, GlobalAveragePooling2D
# from keras.models import Model








def preprocess_image(image, target_size):
	if image.mode != "RGB":
		image = image.convert("RGB")
	#image = padding_that_image(image,target_size)

	desired_size = target_size[0]
	img = np.array(image)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	old_size = img.shape[:2] #np.array(img)
	ratio = float(desired_size)/max(old_size)
	new_size = tuple([int(x*ratio) for x in old_size])
	im = cv2.resize(img, (new_size[1], new_size[0]))
	delta_w = desired_size - new_size[1]
	delta_h = desired_size - new_size[0]
	top, bottom = delta_h//2, delta_h-(delta_h//2)
	left, right = delta_w//2, delta_w-(delta_w//2)

	color = [255, 255, 255] # white padding_that_image
	image = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,value=color)

	print("* hello")
	#image = image.resize(target_size)
	#if norm:
	print("* * * * normalizing....")
	image = image.astype("float") / 255.0 #custom aaron
	print("* 2")
	image = img_to_array(image)
	print("* 3")
	image = np.expand_dims(image,axis=0)

	return image

def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += b'='* (4 - missing_padding)
    return base64.decodestring(data)
