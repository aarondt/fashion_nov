from datetime import datetime
from predict_app import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model,UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20),nullable=False, default='default.jpg') #Images werden gehashed abgespeichert, diese sind dann 20-chars lang.
    password = db.Column(db.String(60), nullable=False) #werden auch gehashed (60 länge)
    #taxonomie = db.relationship('Taxonomie', backref='author', lazy=True)
    uploads = db.relationship('Upload', backref='owner', lazy=True)
    items = db.relationship('Item_Details', backref='user', lazy=True)

    def __repr__(self):
        return "User('{}', '{}', '{}')".format(self.username, self.email, self.image_file)


#class Taxonomie(db.Model):
#    id = db.Column(db.Integer, primary_key=True)
#    title = db.Column(db.String(100),nullable=False)
#    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
#    matching_title = db.Column(db.Text, nullable=False)
#    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
#
#    def __repr__(self):
#        return "User('{}', '{}')".format(self.title, self.date_posted)

class Upload(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100),nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return "Upload('{}', '{}')".format(self.filename, self.date_posted)


class Item_Details(db.Model):
    __tablename__ = "item_table"

    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String(50),nullable=False)
    item_category = db.Column(db.String(50),nullable=True) ## ! Nullable TRUE
    # jede taxonomie soll einem bestimmten user zugeordnet sein
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return "Item_Details('{}', '{}')".format(self.item_name, self.item_category)


class OriginalTaxonomie(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String(50),nullable=False)
    item_category = db.Column(db.String(50),nullable=False)


    def __repr__(self):
        return "Item_Details('{}', '{}')".format(self.item_name, self.item_category)



class Predictions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(50),nullable=False)
    drei_viertel_hosen =  db.Column(db.Float(50),nullable=False)
    Abendkleider =  db.Column(db.Float(50),nullable=False)
    Blusenkleider  =  db.Column(db.Float(50),nullable=False)
    Brautkleider =   db.Column(db.Float(50),nullable=False)
    Bundfaltenhosen = db.Column(db.Float(50),nullable=False)
    Business_Hosen =   db.Column(db.Float(50),nullable=False)
    Chinos =   db.Column(db.Float(50),nullable=False)
    Cocktailkleider =   db.Column(db.Float(50),nullable=False)
    Cordhosen =   db.Column(db.Float(50),nullable=False)
    Culottes =   db.Column(db.Float(50),nullable=False)
    Etuikleider =   db.Column(db.Float(50),nullable=False)
    Haremshosen =   db.Column(db.Float(50),nullable=False)
    Jeanskleider =   db.Column(db.Float(50),nullable=False)
    Jerseykleider =   db.Column(db.Float(50),nullable=False)
    Lederhosen =   db.Column(db.Float(50),nullable=False)
    Leggings =   db.Column(db.Float(50),nullable=False)
    Marlenehosen =   db.Column(db.Float(50),nullable=False)
    Parka =   db.Column(db.Float(50),nullable=False)
    Shorts =   db.Column(db.Float(50),nullable=False)
    Sommerkleider =   db.Column(db.Float(50),nullable=False)
    Spitzenkleider =   db.Column(db.Float(50),nullable=False)
    Steppjacken =   db.Column(db.Float(50),nullable=False)
    Stoffhosen =   db.Column(db.Float(50),nullable=False)
    Strickkleider =   db.Column(db.Float(50),nullable=False)
    Sweatpants =   db.Column(db.Float(50),nullable=False)
    blousons =   db.Column(db.Float(50),nullable=False)
    bomberjacken =   db.Column(db.Float(50),nullable=False)
    cargojacken =   db.Column(db.Float(50),nullable=False)
    jeansjacken =   db.Column(db.Float(50),nullable=False)
    lederjacken =   db.Column(db.Float(50),nullable=False)
    outdoorjacken =   db.Column(db.Float(50),nullable=False)
    regenjacken =   db.Column(db.Float(50),nullable=False)

    ### Hauptkategorien
    jacken	=	db.Column(db.Float(50),nullable=False)
    kleider	=	db.Column(db.Float(50),nullable=False)
    jeans	=	db.Column(db.Float(50),nullable=False)
    strick	=	db.Column(db.Float(50),nullable=False)
    maentel	=	db.Column(db.Float(50),nullable=False)
    blusen_und_tuniken	=	db.Column(db.Float(50),nullable=False)
    hosen	=	db.Column(db.Float(50),nullable=False)
    shirts	=	db.Column(db.Float(50),nullable=False)
    sweat	=	db.Column(db.Float(50),nullable=False)
    pullover	=	db.Column(db.Float(50),nullable=False)
    waesche	=	db.Column(db.Float(50),nullable=False)
    roecke	=	db.Column(db.Float(50),nullable=False)
    blazer	=	db.Column(db.Float(50),nullable=False)
    bademode	=	db.Column(db.Float(50),nullable=False)
    jumpsuits_und_overalls	=	db.Column(db.Float(50),nullable=False)
    tops	=	db.Column(db.Float(50),nullable=False)
    westen	=	db.Column(db.Float(50),nullable=False)
    shorts_hose	=	db.Column(db.Float(50),nullable=False)
    ponchos_und_kimonos	=	db.Column(db.Float(50),nullable=False)




    def __repr__(self):
        return "Item_Details('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(self.filename,
self.drei_viertel_hosen,
self.Abendkleider,
self.Blusenkleider,
self.Brautkleider,
self.Bundfaltenhosen,
self.Business_Hosen,
self.Chinos,
self.Cocktailkleider,
self.Cordhosen,
self.Culottes,
self.Etuikleider,
self.Haremshosen,
self.Jeanskleider,
self.Jerseykleider,
self.Lederhosen,
self.Leggings,
self.Marlenehosen,
self.Parka,
self.Shorts,
self.Sommerkleider,
self.Spitzenkleider,
self.Steppjacken,
self.Stoffhosen,
self.Strickkleider,
self.Sweatpants,
self.blousons,
self.bomberjacken,
self.cargojacken,
self.jeansjacken,
self.lederjacken,
self.outdoorjacken,
self.regenjacken)
#df.to_sql(name='item_table', con=db.engine, index=False,if_exists='replace')
